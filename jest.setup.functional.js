// @todo use 'get-port' or similar to get any free port, enable running tests in parallel
const apiPort = 8123

module.exports = {
  setupFilesAfterEnv: ['./test/setup.js'],
  automock: false,
  globals: {
    __PORT__: apiPort,
    __ENDPOINT__: `http://localhost:${apiPort}`
  }
}
