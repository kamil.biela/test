exports.ValueStoreRepository = class ValueStoreRepository {
  /**
   * @param {import('sqlite3').Database} db
   */
  constructor(db) {
    this.db = db
    // add query builder like 'squel' (https://hiddentao.github.io/squel/)
  }

  /**
   * @param {string} id
   * @returns {Promise<*[]>}
   */
  async findById(id) {
    return new Promise((resolve, reject) => {
      const stmt = this.db.prepare(
        'SELECT * FROM value_store where id = (?)',
        err => {
          if (err) {
            return reject(err)
          }
        }
      )
      stmt.all(id, (err, rows) => {
        if (err) {
          return reject(err)
        }

        return resolve(rows)
      })
      stmt.finalize()
    })
  }

  async findByLikeId(id) {
    id = id.replace('*', '%%')

    return new Promise((resolve, reject) => {
      const stmt = this.db.prepare(
        'SELECT * FROM value_store WHERE id LIKE ?',
        err => {
          if (err) {
            return reject(err)
          }
        }
      )
      stmt.all(id, (err, rows) => {
        if (err) {
          return reject(err)
        }

        return resolve(rows)
      })
      stmt.finalize()
    })
  }

  async upsert(id, value) {
    return new Promise((resolve, reject) => {
      const stmt = this.db.prepare(
        `
        INSERT INTO value_store (id, val)
        VALUES (?, ?)
        ON CONFLICT(id) DO UPDATE SET val = excluded.val
      `,
        err => {
          if (err) {
            return reject(err)
          }
        }
      )
      stmt.run(id, value, err => {
        if (err) {
          return reject(err)
        }

        return resolve()
      })
      stmt.finalize()
    })
  }
}
