/* eslint-disable no-undef */

const CryptoService = require('./crypto-service').CryptoService

describe('crypto-service.js', () => {
  /** @tpye {CryptoService} */
  let cryptoService

  beforeEach(() => {
    cryptoService = new CryptoService()
  })

  it('must be possible to encrypt and decrypt passed value', async () => {
    const value = 'some string value !@#$%^&*()-='
    const pass = 'some password'

    const encrypted = await cryptoService.encrypt(value, pass)
    const decrypted = await cryptoService.decrypt(encrypted, pass)

    expect(decrypted).toBe(value)
  })

  it('must be impossible to decrypt passed value if password is wrong', async () => {
    const value = 'some string value !@#$%^&*()-='
    const pass = 'some password'

    const encrypted = await cryptoService.encrypt(value, pass)

    return expect(
      cryptoService.decrypt(encrypted, 'otherpass')
    ).rejects.toBeTruthy()
  })
})
