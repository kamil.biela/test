/**
 * @todo create Logger interface and make it configurable in config.js
 * @todo create constructor which accepts log level parameter, to enable / disable some messages
 */
exports.ConsoleLogger = class ConsoleLogger {
  /**
   * @param {*} message
   */
  async log(message) {
    console.log(message)
  }

  /**
   * @param {*} message
   */
  async error(message) {
    console.error('ERROR: ', message)
  }
}
