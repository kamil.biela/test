exports.ValueStoreService = class ValueStoreService {
  /**
   * @param {import('./repository/value-store-repository').ValueStoreRepository} valueStoreRepository
   * @param {import('./crypto-service').CryptoService} cryptoService
   * @param {import('./console-logger').ConsoleLogger} logger
   */
  constructor(valueStoreRepository, cryptoService, logger) {
    this.valueStoreRepository = valueStoreRepository
    this.cryptoService = cryptoService
    this.logger = logger
  }

  /**
   * @param {string} id
   * @param {string} password
   * @returns {Promise<*[]>}
   */
  async getForId(id, password) {
    let values = []

    if (id.indexOf('*') !== -1) {
      // @todo add safeguard so it's not possible to fetch too many records
      values = await this.valueStoreRepository.findByLikeId(id)
    } else {
      values = await this.valueStoreRepository.findById(id)
    }

    let decryptedValues = []

    try {
      for (const v of values) {
        const decryptedValue = await this.cryptoService.decrypt(v.val, password)
        decryptedValues.push({
          ...v,
          val: JSON.parse(decryptedValue)
        })
      }
    } catch (e) {
      decryptedValues = []
      await this.logger.log(`Wrong password for record`)
    }

    return decryptedValues
  }

  /**
   * @param {string} id
   * @param {string} value
   * @returns {Promise<undefined>}
   */
  async putAtId(id, value, password) {
    const stringifiedValue = JSON.stringify(value)
    const encryptedValue = await this.cryptoService.encrypt(
      stringifiedValue,
      password
    )
    return this.valueStoreRepository.upsert(id, encryptedValue)
  }
}
