const crypto = require('crypto')

exports.CryptoService = class CryptoService {
  constructor() {
    this.algorithm = 'aes-192-cbc'
  }

  /**
   * This piece of code is more or less copy/paste from nodejs documentation.
   * All coments left as in documentation
   * Definetely not production ready and uses sync functions
   * @param {string} plainValue
   * @param {string} password
   * @returns {Promise<string>} encrypted string
   */
  async encrypt(plainValue, password) {
    // Use the async `crypto.scrypt()` instead.
    const key = crypto.scryptSync(password, 'salt', 24)
    // Use `crypto.randomBytes` to generate a random iv instead of the static iv
    // shown here.
    const iv = Buffer.alloc(16, 0) // Initialization vector.
    const cipher = crypto.createCipheriv(this.algorithm, key, iv)
    let encrypted = cipher.update(plainValue, 'utf8', 'hex')
    encrypted += cipher.final('hex')

    return encrypted
  }

  /**
   * As above, copy/paste from doc
   *
   * @param {string} encryptedValue
   * @param {string} password
   * @returns {Promise<string>} decrypted string
   */
  async decrypt(encryptedValue, password) {
    return new Promise(resolve => {
      // Key length is dependent on the algorithm. In this case for aes192, it is
      // 24 bytes (192 bits).
      // Use the async `crypto.scrypt()` instead.
      const key = crypto.scryptSync(password, 'salt', 24)
      // The IV is usually passed along with the ciphertext.
      const iv = Buffer.alloc(16, 0) // Initialization vector.

      const decipher = crypto.createDecipheriv(this.algorithm, key, iv)

      let decrypted = ''
      let chunk
      decipher.on('readable', () => {
        while ((chunk = decipher.read()) !== null) {
          decrypted += chunk.toString('utf8')
        }
      })
      decipher.on('end', () => {
        return resolve(decrypted)
      })

      // Encrypted with same algorithm, key and iv.
      decipher.write(encryptedValue, 'hex')
      decipher.end()
    })
  }
}
