/* eslint-disable no-undef */

const valueStore = require('./value-store')

describe('value-store.js', () => {
  describe('getGetDataRoute', () => {
    let params
    let headers

    beforeEach(() => {
      params = {}
      headers = { 'decryption-key': 'key' }
    })

    // @todo add more tests that are more convienent to test by unit testing than functional

    it('should pass validation error to next, when id param contains forbidden char', done => {
      const container = {}
      const route = valueStore.getGetDataRoute(container)[0]
      const mockNext = jest.fn()

      const req = {
        params: {
          ...params,
          id: 'key-*'
        },
        headers
      }

      route(req, {}, mockNext)
      setTimeout(() => {
        expect(mockNext.mock.calls.length).toBe(1)
        expect(mockNext.mock.calls[0][0].message).toBe('Invalid format for id')
        done()
      }, 0)
    })
  })
})
