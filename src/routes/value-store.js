const yup = require('yup')

/**
 * @param {import('../build-container').Container} container
 * @returns {import('express').RequestHandler[]}
 */
exports.getGetDataRoute = function getGetDataRoute(container) {
  // @todo if needed move validations to separate module / service
  // @todo as per email, ensure that there is only one '*' character in id param
  const validatePathParamsSchema = yup.object().shape({
    id: yup
      .string()
      .required()
      .matches(/^[A-Za-z0-9_.\\*]+$/, 'Invalid format for id')
  })

  const validateHeadersSchema = yup.object().shape({
    'decryption-key': yup.string().required()
  })

  // return array of route handlers, so every function configuring route has common interface
  // allows to pass custom middlewares to route definitions, as app.get and other accept array
  // as second arg
  return [
    (req, res, next) => {
      ;(async () => {
        await validatePathParamsSchema.validate(req.params)
        await validateHeadersSchema.validate(req.headers)

        let decryptionKeyHeader = req.headers['decryption-key']

        if (Array.isArray(decryptionKeyHeader)) {
          decryptionKeyHeader = decryptionKeyHeader[0]
        }

        const data = await container.valueStore.getForId(
          req.params.id,
          decryptionKeyHeader
        )

        res.json(data)
      })().catch(next)
    }
  ]
}

/**
 * @param {import('../build-container').Container} container
 * @returns {import('express').RequestHandler[]}
 */
exports.getPutDataRoute = function getPutDataRoute(container) {
  // @todo if needed move validations to separate module / service
  const validatePathParamsSchema = yup.object().shape({
    id: yup
      .string()
      .required()
      .matches(/^[A-Za-z0-9_.]+$/, 'Invalid format for id')
  })

  const validateBodySchema = yup.object().shape({
    encryption_key: yup.string().required(),
    value: yup.mixed().nullable()
  })

  return [
    (req, res, next) => {
      ;(async () => {
        await validatePathParamsSchema.validate(req.params)
        await validateBodySchema.validate(req.body)

        await container.valueStore.putAtId(
          req.params.id,
          req.body.value,
          req.body.encryption_key
        )

        res.status(200).send('')
      })().catch(next)
    }
  ]
}
