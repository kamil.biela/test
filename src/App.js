const express = require('express')
const routes = require('./routes/index.js')
const assert = require('assert')
const yup = require('yup')
const bodyParser = require('body-parser')

exports.App = class App {
  /**
   *
   * @param {import('./build-container').Container} container
   */
  constructor(container) {
    assert.ok(container)

    this.container = container

    /** @type {(express.Express | undefined)} */
    this.app = undefined

    /** @type {(import('http').Server | undefined)} */
    this.server = undefined
  }

  /**
   * @private
   */
  configureRoutes() {
    if (!this.app) {
      throw new Error('this.app not initialized')
    }

    this.app.get('/data/:id', routes.getGetDataRoute(this.container))
    this.app.put('/data/:id', routes.getPutDataRoute(this.container))
  }

  /**
   * @private
   */
  async initHttp() {
    if (this.server) {
      throw new Error('Http server already initialized')
    }

    // await to init connection to db / configure connection pool

    return new Promise(resolve => {
      this.app = express()
      this.app.use(bodyParser.json())

      this.configureRoutes()

      this.app.use((err, req, res, next) => {
        if (res.headersSent) {
          return next(err)
        }

        if (err instanceof yup.ValidationError) {
          return res.status(400).json(err)
        } else if (err) {
          // @todo add nice generic error handler
          // return in response when config flag is set (ie. for test env)
          // send status code 500 in prod and only log the error
          this.container.logger.error(err)
        }

        return next(err)
      })

      this.server = this.app.listen(this.container.config.httpPort, resolve)
    })
  }

  /**
   * @private
   */
  async closeDb() {
    return new Promise(resolve => {
      this.container.db.close(err => {
        if (err) {
          this.container.logger.error(err)
        }

        return resolve()
      })
    })
  }

  /**
   * @private
   */
  async stopHttp() {
    return new Promise(resolve => {
      this.server.close(resolve)
    })
  }

  async start() {
    return this.initHttp()
  }

  async stop() {
    if (!this.app) {
      return
    }

    try {
      await this.stopHttp()
    } finally {
      await this.closeDb()
    }
  }
}
