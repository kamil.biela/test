const App = require('./App.js').App
const buildContainer = require('./build-container').buildContainer
const loadConfig = require('./load-config').loadConfig

/**
 * @param {import('../config.js').Config} config
 * @returns {Promise<*>}
 */
const initApp = async config => {
  const container = await buildContainer(config)

  const app = new App(container)

  return app.start()
}

loadConfig()
  .then(config => initApp(config))
  .catch(e => {
    console.error(e)
    process.exit(1)
  })
