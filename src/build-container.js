const ConsoleLogger = require('./service/console-logger').ConsoleLogger
const CryptoService = require('./service/crypto-service').CryptoService
const sqlite3 = require('sqlite3').verbose()
const ValueStoreRepository = require('./service/repository/value-store-repository')
  .ValueStoreRepository
const ValueStoreService = require('./service/value-store-service')
  .ValueStoreService

/**
 * @typedef {Object} Container
 * @property {import('../config.js').Config} config
 * @property {ConsoleLogger} logger
 * @property {CryptoService} crypto
 * @property {import('sqlite3').Database} db
 * @property {ValueStoreService} valueStore
 */

/**
 * See comment in buildContainer
 * @param {import('sqlite3').Database} db
 */
async function loadSchema(db) {
  db.serialize(function() {
    db.run(`CREATE TABLE value_store (
        id TEXT PRIMARY KEY,
        val TEXT
      )
    `)
  })
}

/**
 * Build app DI Container
 *
 * @param {import('../config.js').Config} config
 * @return {Promise<Container>}
 */
exports.buildContainer = async function buildContainer(config) {
  // @todo depending on config value, create requested logger service instance
  const loggerService = new ConsoleLogger()
  const cryptoService = new CryptoService()

  const db = new sqlite3.Database(':memory:')
  // @todo this should be separate module, that can be used via cli and programmatically
  loadSchema(db)

  const valueStoreRepository = new ValueStoreRepository(db)
  const valueStoreService = new ValueStoreService(
    valueStoreRepository,
    cryptoService,
    loggerService
  )

  // dont put repositories to container, so that way it's possible to force controllers
  // to use only services to access / store data
  // treat repositories as private services
  return {
    config,
    crypto: cryptoService,
    logger: loggerService,
    db,
    valueStore: valueStoreService
  }
}
