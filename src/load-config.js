const config = require('../config.js')

/**
 * Loads config.
 *
 * Using async to easily enable loading config from other sources than filesystem / env variables, if needed.
 *
 * @returns {Promise<import('../config.js').Config>}
 */
exports.loadConfig = async function loadConfig() {
  return config
}
