/**
 * @typedef {Object} Config
 * @property {number} httpPort
 */

/** @type {Config} */
module.exports = {
  httpPort: parseInt(process.env.HTTP_PORT) || 8080
}
