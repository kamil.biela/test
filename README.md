# About code
- JSDoc to document / typecheck as much as possible - recently I'm using a lot of typescript and I wanted to see what's possible with pure js
- DI pattern to enable easy testing, I'm no fan of rewire and similar
- I'm usually using mocha, but I wanted to try out jest, so it might not be configured in the best way

# About used tools
- yarn - tends to be faster than npm, but no yarn specific functionality used here
- eslint / prettier / typescript types - to ensure consistent code style, catch //common coding errors and check types
- .editorconfig to pass basic formatting rules to supported editors

# Concerns/issues that need addressing
- rate limiting for store / retrieve endpoints. It's much easier to DOS the service by taking 100% CPU, because of encryption/decryption takes place.
- service/crypto-service.js has copy-paste code from node.js documentation and it has a couple of issues

# Next steps to improve code
- see comments in code for smaller issues
- use something better than in memory sqlite
- put docker-compose.yml with database for easy local dev env
- dockerize app to allow easy deploy to production/staging
- configure automatic tests with CircleCi or similar

# Install
```bash
yarn install
```

# Config
Optional env vars:
- `HTTP_PORT` - define on which port start service, default is `8080`

# Run service
```bash
yarn start
```

# Other commands
```yarn run check:all``` - run all code quality checking tools
See package.json for more
