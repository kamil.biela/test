const request = require('request-promise-native')

exports.request = request.defaults({
  resolveWithFullResponse: true,
  json: true,
  simple: false
})
