const request = require('request-promise-native')
const App = require('../src/App').App
const buildContainer = require('../src/build-container').buildContainer

/** @type App */
let app

beforeEach(async function() {
  // @todo:
  // 1 clear db
  // 2 load schema / migrations
  // 3 load test fixtures
  // additional:
  // - dump db to var / buffer
  // - use that buffer in step 2, instead of 'nomral' operation

  /** @type {import('../src/build-container').Config} */
  const config = {
    httpPort: global.__PORT__
  }

  const container = await buildContainer(config)
  app = new App(container)

  return app.start()
})

afterEach(async function() {
  return app.stop()
})
