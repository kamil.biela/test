const request = require('./helpers').request

describe('value store endpoint test', function() {
  it('must return error when decryption-key is not present in header', async () => {
    const result = await request(`${global.__ENDPOINT__}/data/val1`)
    expect(result.statusCode).toBe(400)
    expect(result.body.message).toBe('decryption-key is a required field')
  })

  it('must return empty list when data is not found for given key', async () => {
    const result = await request(`${global.__ENDPOINT__}/data/val1`, {
      headers: {
        // @todo key ideally should be taken from fixtures const
        'decryption-key': 'key'
      }
    })

    expect(result.statusCode).toBe(200)
    expect(result.body).toStrictEqual([])
 })

  // @todo next tests should be split into multiple ones and cases should be handled by fixtures
  // because one test is checking both endpoints
  it('must be able to save and retrieve the value', async () => {
    const val = ['a', 'b']
    await request(`${global.__ENDPOINT__}/data/val1`, {
      method: 'put',
      body: {
        encryption_key: 'key1',
        value: val
      }
    })

    const result = await request(`${global.__ENDPOINT__}/data/val1`, {
      headers: {
        'decryption-key': 'key1'
      }
    })

    expect(result.statusCode).toBe(200)
    expect(result.body).toStrictEqual([{id: 'val1', val}])
  })

  it('must be able to save and retrieve multiple values by using * in id', async () => {
    for (const v of ['a', 'b', 'c']) {
      await request(`${global.__ENDPOINT__}/data/val1${v}`, {
        method: 'put',
        body: {
          encryption_key: 'key1',
          value: v
        }
      })
    }

    const result1 = await request(`${global.__ENDPOINT__}/data/val1*`, {
      headers: {
        'decryption-key': 'key1'
      }
    })

    expect(result1.statusCode).toBe(200)
    expect(result1.body).toStrictEqual([
      { id: 'val1a', val: 'a' },
      { id: 'val1b', val: 'b' },
      { id: 'val1c', val: 'c' }
    ])

    const result2 = await request(`${global.__ENDPOINT__}/data/v*a`, {
      headers: {
        'decryption-key': 'key1'
      }
    })
    expect(result2.body).toStrictEqual([{ id: 'val1a', val: 'a' }])

    const result3 = await request(`${global.__ENDPOINT__}/data/*c`, {
      headers: {
        'decryption-key': 'key1'
      }
    })
    expect(result3.body).toStrictEqual([{ id: 'val1c', val: 'c' }])
  })

  it('must return empty list when any decryption fails', async () => {
    for (const v of ['a', 'b']) {
      await request(`${global.__ENDPOINT__}/data/val1${v}`, {
        method: 'put',
        body: {
          encryption_key: `key${v}`,
          value: v
        }
      })
    }

    const result1 = await request(`${global.__ENDPOINT__}/data/val1*`, {
      headers: {
        'decryption-key': 'keya'
      }
    })

    expect(result1.statusCode).toBe(200)
    expect(result1.body).toStrictEqual([])
  })
})
